# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###
FileTransformer application to transform fixed width and multi schema files of CreditSafe.
### How do I get set up? ###

The utility works for Postnummer,ATX and Unireg

Usage:
spark-submit FileTransformer.jar action path
action could be either transform or lineage


To transform the files and for the lineage to be visible in the UI between the original and transformed files,the order of executing the commands is

1.run FileTransformer transform
2.run waterline_workflow hdfs
3.*run FileTransformer  lineage 

*FileTransformer lineage should not be confused with the waterline command ‘lineage’.


1.)The user runnning this command must have write permissions on hdfs/s3 as the files are transformed and written to the existing folder

 Ex: spark-submit FileTransformer.jar transform s3://my-bucket-name
You may give the root path ‘/’ instead of s3://my-bucket-name if the defaultFS value in core-site.xml has been configured to point to your s3 bucket


2.)The resources are now ready and their resource id's need to be populated in solr. This is required, as we need the resource id's for creating the lineage through REST API between the original and transformed files

 Now run the waterline_workflow command  and once the status of the folder is processed we can go ahead and create the lineage for the original and the transformed files. 


 Ex: waterline_workflow hdfs -dataSource MyS3Bucket -path inputFiles


3.)To populate the lineage information in the WaterlineData Smart Catalog, run the FileTransformer’s lineage command
 
 Ex: spark-submit FileTransformer.jar lineage s3://my-bucket-name
You may give the root path ‘/’ instead of s3://my-bucket-name if the defaultFS value in core has been configured to point to your s3 bucket

#Configuration 
config.properties file
contains the configurable values for file schema and Waterlinedata Smart Catalog REST API Client

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact