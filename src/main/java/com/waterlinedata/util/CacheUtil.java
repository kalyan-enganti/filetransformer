package com.waterlinedata.util;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.List;

import org.apache.commons.io.IOUtils;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.MapFile;
import org.apache.hadoop.io.Text;
import org.apache.log4j.Logger;

public class CacheUtil {
	private final static Logger LOGGER = Logger.getLogger(CacheUtil.class.getName());
	static String location = "hdfs://" + FileUtils.getHostName() + ":8020/user/"+PropertiesUtil.getPropertyValue("wd.service.user")+"/.transformed_files";
	static Configuration conf = new Configuration();
	static FileSystem fs = null;
	static LineageUtil lUtil = new LineageUtil();

	@SuppressWarnings("deprecation")
	public static synchronized void writeToCache(List<String> recordsList) {

		try {
			fs = FileSystem.get(conf);

			Path outputFile = new Path(location);
			Text txtKey = new Text();
			Text txtValue = new Text();

			String lstKeyValuePair[] = null;
			MapFile.Writer writer = null;

			try {
				writer = new MapFile.Writer(conf, fs, outputFile.toString(), txtKey.getClass(), txtValue.getClass());
				writer.setIndexInterval(1);
				for (String str : recordsList) {
					lstKeyValuePair = str.split("\\t");
					txtKey.set(FileUtils.getPath(lstKeyValuePair[0]));
					txtValue.set(FileUtils.getPath(lstKeyValuePair[1]));
					writer.append(txtKey, txtValue);
				}

			} finally {
				IOUtils.closeQuietly(writer);
				LOGGER.info("Entries created successfully!!");
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	

	public static synchronized void readFromCache(String str) {
		str=FileUtils.getPath(str);
		Text txtKey = new Text(str);
		Text txtValue = new Text();
		MapFile.Reader reader = null;

		try {
			fs = FileSystem.get(conf);

			try {
				reader = new MapFile.Reader(fs, location, conf);
				reader.get(txtKey, txtValue);
			} catch (IOException e) {
				e.printStackTrace();
			}

		} catch (IOException e) {
			e.printStackTrace();
		}
		if (txtValue.toString().length() > 1) {
			String[] values = txtValue.toString().split(",");
			for (String val : values) {
				if (val.length() > 1) {
					try {
						LOGGER.info("The value for Key " + txtKey.toString() + " is " + txtValue.toString());
						LOGGER.info("Creating Lineage with the above values");
						lUtil.processLineage(txtKey.toString(), val);
					} catch (URISyntaxException e) {
						LOGGER.error(" URIException in reading from cache");
						e.printStackTrace();
					}
				}

			}

		}

	}
}
