package com.waterlinedata.util;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.Console;
import java.io.FileWriter;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.nio.ByteBuffer;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.NoSuchAlgorithmException;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SealedObject;
import javax.crypto.spec.SecretKeySpec;
import javax.xml.bind.DatatypeConverter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Simple symmetrical encryptor/decryptor with rudimentary intercept protection.
 * <p/>
 * The produced encrypted text is a BASE64 encoded string where the first 8
 * bytes contain a timestamp (long System.currentTimeMillis()) used to modify a
 * fixed secret key during encryption. The rest of the token contains a sealed
 * object (string) with a secret text. Reverse key modification must take place
 * during decryption.
 * <p/>
 * @author Azary Smotrich
 */
@SuppressWarnings("ClassWithoutLogger")
public class Cryptofer
{
    private static final Logger LOG =
            LoggerFactory.getLogger(Cryptofer.class);

    private static final String KEY = "2JqZWN0PjY9psO3VHACAARbAA1lbmNvZGVkUGFy";
    private static final String AES = "AES";
    private static final String XFM = "AES/ECB/PKCS5Padding";
    private static final String ENC = "UTF-8";
    private static final int    KEY_LENGTH = 16;

    private Cryptofer()
    {
    }

    /**
     * Encrypt arbitrary clear text.
     * <p/>
     * @param clearText clear text {@code String} to be encrypted
     * <p/>
     * @return BASE64 encoded sealed encrypted secret text
     */
    public static String encrypt(String clearText)
    {

        if (clearText == null) {
            return null;
        }
        String encriptedB64 = null;
        ObjectOutputStream oos = null;
        try
        {
            ByteBuffer kbb = ByteBuffer.wrap(KEY.getBytes(ENC), 0, KEY_LENGTH);

            long millis = System.currentTimeMillis();
            ByteBuffer tbb = ByteBuffer.allocate(KEY_LENGTH);
            while (tbb.position() < kbb.limit())
            {
                tbb.putLong(millis);
            }
            tbb.rewind();

            ByteBuffer kbx = ByteBuffer.allocate(KEY_LENGTH);
            while (kbx.remaining() > 0)
            {
                kbx.put((byte) (kbb.get() ^ tbb.get()));
            }
            kbx.rewind();

            Key key = new SecretKeySpec(kbx.array(), 0, KEY_LENGTH, AES);
            Cipher cipher = Cipher.getInstance(XFM);
            cipher.init(Cipher.ENCRYPT_MODE, key);

            SealedObject so = new SealedObject(clearText, cipher);

            // NIO Buffer family lacks very convenient writeObject method
            // used here, hence a bit of legacy classes usage.
            ByteArrayOutputStream baos = new ByteArrayOutputStream(512);
            oos = new ObjectOutputStream(baos);
            oos.writeLong(millis);
            oos.writeObject(so);
            encriptedB64 = DatatypeConverter.printBase64Binary(baos.toByteArray());
        }
        catch (InvalidKeyException |
                NoSuchAlgorithmException |
                NoSuchPaddingException |
                IllegalBlockSizeException |
                IOException ex)
        {
            LOG.error("Exception", ex);
        }
        finally
        {
            if (oos != null)
            {
                try
                {
                    oos.close();
                }
                catch (IOException ex)
                {
                    LOG.error("Exception", ex);
                }
            }

        }
        return encriptedB64;
    }

    /**
     * Decrypt given cipher text that is assumed to have been encrypted by this
     * class {@code encrypt} method.
     * <p>
     * @param encryptedText encrypted sealed BASE64 encoded text {@code String}
     * <p>
     * @return decrypted text or {@code null} in case of errors
     */
    public static String decrypt(String encryptedText)
    {

        if (encryptedText == null || encryptedText.length() < 128) {
            return encryptedText;
        }
        String result = null;
        ObjectInputStream ois = null;
        try
        {
            // Assume the token is the Base64 encoded SealedObject preceded
            // by the timestamp used during encryption.
            // NIO Buffer family lacks very convenient readObject method
            // used here, hence a bit of legacy classes usage.
            byte[] buf = DatatypeConverter.parseBase64Binary(encryptedText);
            ois = new ObjectInputStream(new ByteArrayInputStream(buf));
            long millis = ois.readLong();
            Object obj = ois.readObject();
            if (obj instanceof SealedObject)
            {
                SealedObject sobj = (SealedObject) obj;

                ByteBuffer kbb = ByteBuffer.wrap(KEY.getBytes(ENC), 0, KEY_LENGTH);
                ByteBuffer tbb = ByteBuffer.allocate(KEY_LENGTH);
                while (tbb.position() < kbb.limit())
                {
                    tbb.putLong(millis);
                }
                tbb.rewind();

                ByteBuffer kbx = ByteBuffer.allocate(KEY_LENGTH);
                while (kbx.remaining() > 0)
                {
                    kbx.put((byte) (kbb.get() ^ tbb.get()));
                }
                kbx.rewind();

                Key key = new SecretKeySpec(kbx.array(), 0, KEY_LENGTH, AES);
                Cipher cipher = Cipher.getInstance(XFM);
                cipher.init(Cipher.DECRYPT_MODE, key);

                result = (String) sobj.getObject(cipher);
            }
            else
            {
                LOG.error("Wrong object deserialized: {}",
                        obj.getClass().getName());
            }
        }
        catch (InvalidKeyException |
                NoSuchAlgorithmException |
                NoSuchPaddingException |
                IllegalBlockSizeException |
                BadPaddingException |
                ClassNotFoundException |
                IOException ex)
        {
            LOG.error("Exception", ex);
        }
        finally
        {
            if (ois != null)
            {
                try
                {
                    ois.close();
                }
                catch (IOException ex)
                {
                    LOG.error("Exception", ex);
                }
            }

        }
        return result;
    }

    /**
     * Verify that the encrypted text represents the given plain text.
     * <p/>
     * @param plainText     clear un-encrypted text {@code String}
     * @param encryptedText encrypted sealed BASE64 encoded text {@code String}
     * <p/>
     * @return {@code true} if encrypted text produces exactly the clear text
     *         after decryption, {@code false} otherwise
     */
    public static boolean verify(String plainText, String encryptedText)
    {
        return plainText != null && encryptedText != null &&
                plainText.equals(decrypt(encryptedText));
    }

    @SuppressWarnings("UseOfSystemOutOrSystemErr")
    public static void main(String[] args) throws IOException
    {
        Console console = System.console();
        if (args.length == 0)
        {
            if (console != null)
            {
                if (args.length == 0)
                {
                    char[] pwd = console.readPassword("%s: ", "Enter text");
                    String enc = Cryptofer.encrypt(new String(pwd));
                    enc = enc.replace("\r\n", "");
                    console.format("%s\n", enc);
                    try (FileWriter writer = new FileWriter("obfuscate.out"))
                    {
                        writer.append(enc);
                        writer.flush();
                    }
                }
            }
            else
            {
                System.err.println("Cannot open the console");
            }
        }
        else if (args.length == 1)
        {
            // hidden decryption mode
            String secret = args[0];
            String plain = Cryptofer.decrypt(secret);
            if (console != null)
            {
                console.format("%s\n", plain);
            }
            else
            {
                System.out.println(plain);
            }
        }
    }
}
