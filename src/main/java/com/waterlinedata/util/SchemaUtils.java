package com.waterlinedata.util;

import java.io.File;

import org.apache.log4j.Logger;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.function.Function;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.RowFactory;

import com.waterlinedata.entity.Constants;

public class SchemaUtils {
	private final static Logger LOGGER = Logger.getLogger(SchemaUtils.class.getName());
	private static String colNames[];
	private static Integer fieldLengths[];

	public static String[] getColNames() {
		return colNames;
	}

	public static Integer[] getFieldLengths() {
		return fieldLengths;
	}

	public static String[] getFileSchemaData(String type, String value,String file) {
		LOGGER.info("Loading SchemaData  for " + type);
		String[] schemaData = null;
		if (type.equalsIgnoreCase(Constants.FileType_POSTNUMMER)) {
			schemaData = new String[2];
			schemaData[0] = PropertiesUtil.getPropertyValue("Postnummer.field.names");
			schemaData[1] = PropertiesUtil.getPropertyValue("Postnummer.field.lengths");
		} else if (type.equals(Constants.FileType_ATX)) {
			schemaData = new String[2];
			schemaData[0] = PropertiesUtil.getPropertyValue("ATX." + value + ".field.names");
			schemaData[1] = PropertiesUtil.getPropertyValue("ATX." + value + ".field.lengths");
			if (null == schemaData[0] || null == schemaData[1]) {
				LOGGER.error("Expected property for ATX." + value);
				throw new RuntimeException("Schema information not found for ATX year-> " + value);
			}
		} else if (type.equalsIgnoreCase(Constants.FileType_UNIREG)) {
			schemaData = new String[2];
			schemaData[0] = PropertiesUtil.getPropertyValue("UNIREG." + value + "."+FileUtils.getParentFolder(file)+".field.names");
			schemaData[1] = PropertiesUtil.getPropertyValue("UNIREG." + value + "."+FileUtils.getParentFolder(file)+".field.lengths");
			if (null == schemaData[0] || null == schemaData[1]) {
				LOGGER.error("Expected property for UNIREG." + value);
				throw new RuntimeException("Schema information not found for UNIREG post_type-> " + value);
			}
		}
		return schemaData;
	}

	public static void setFieldLengthsAndNames(String[] schemaData) {
		LOGGER.info("Setting FieldLengths and Names");
		if (schemaData.length == 1) {
			if (null != schemaData[0]) {
				String fLengths[] = schemaData[0].split(",");
				fieldLengths = new Integer[fLengths.length];
				for (int j = 0; j < fieldLengths.length; j++) {
					fieldLengths[j] = Integer.parseInt(fLengths[j]);
				}
			}
		} else if (schemaData.length == 2) {
			if (schemaData[0].split(",").length != schemaData[1].split(",").length) {
				System.out.println("Fields and lengths mismatch\nExiting");
				System.exit(-1);
			}

			if (null != schemaData[0]) {
				String fields[] = schemaData[0].split(",");
				colNames = new String[fields.length];
				for (int j = 0; j < colNames.length; j++) {
					colNames[j] = fields[j];
				}

			}
			if (null != schemaData[1]) {
				String fLengths[] = schemaData[1].split(",");
				fieldLengths = new Integer[fLengths.length];
				for (int j = 0; j < fieldLengths.length; j++) {
					fieldLengths[j] = Integer.parseInt(fLengths[j]);
				}
			}
		} else if (schemaData.length == 0) {
			System.out.println("Field lengths are required to transform files\nExiting");
			System.exit(-1);
		}

	}

	@SuppressWarnings("serial")
	public static synchronized JavaRDD<Row> createRDD(final String type, final boolean isInitial, JavaRDD<String> input,
			final Integer fieldLengths[], String colNames[]) {
		LOGGER.info("Creating RDD  for " + type + " isInitial " + isInitial);
		JavaRDD<Row> rowRDD = input.map(new Function<String, Row>() {
			public Row call(String record) throws Exception {
				String[] fields = new String[fieldLengths.length];
				for (int i = 0; i < fields.length; i++) {
					if (i == 0 && !type.equals(Constants.FileType_UNIREG)) {
						fields[i] = record.substring(0, fieldLengths[i]).trim();
					} else if (i == 0 && type.equals(Constants.FileType_UNIREG)) {
						fields[i] = record.substring(0, fieldLengths[i]);
					} else if ((i > 0 && type.equalsIgnoreCase(Constants.FileType_POSTNUMMER))) {
						fields[i] = record.substring(getStartPos(fieldLengths, i), getEndPos(fieldLengths, i)).trim();
					} else if (i > 0 && type.equals(Constants.FileType_ATX) && isInitial) {
						fields[i] = record.substring(2, record.length());
					} else if (i > 0 && type.equals(Constants.FileType_ATX) && !isInitial) {
						fields[i] = record.substring(getStartPos(fieldLengths, i), getEndPos(fieldLengths, i)).trim();
					} else if (i > 0 && type.equals(Constants.FileType_UNIREG) && isInitial) {
						if (i == 1)
							fields[i] = record.substring(66, 69);
						if (i == 2)
							fields[i] = record.substring(69, record.length());
					} else if (i > 0 && type.equals(Constants.FileType_UNIREG) && !isInitial) {
						fields[i] = record.substring(getStartPos(fieldLengths, i), getEndPos(fieldLengths, i)).trim();
					}
				}

				return RowFactory.create(fields);
			}

			private int getStartPos(Integer[] fieldLengths, int i) {
				int pos = 0;
				for (int j = 0; j < i; j++) {
					pos += fieldLengths[j];
				}
				return pos;
			}

			private int getEndPos(Integer[] fieldLengths, int i) {
				int pos = 0;
				for (int j = 0; j <= i; j++) {
					pos += fieldLengths[j];
				}
				return pos;
			}
		});
		return rowRDD;

	}
}
