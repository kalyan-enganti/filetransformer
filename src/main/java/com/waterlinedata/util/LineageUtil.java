package com.waterlinedata.util;

import java.net.URISyntaxException;
import java.util.List;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.NewCookie;

import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONObject;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;

public  class LineageUtil {
	private final static Logger LOGGER = Logger.getLogger(LineageUtil.class
			.getName());
	private static NewCookie sSessionId = null;
	private static Client client=null;
	private static String sProtocol = PropertiesUtil.getPropertyValue("wd.protocol");
	private static String sWebUIHost = PropertiesUtil.getPropertyValue("wd.host");
	private static String sPort = PropertiesUtil.getPropertyValue("wd.port");
	private static String sUser =PropertiesUtil.getPropertyValue("wd.user");
	private static String sPwd = PropertiesUtil.getPropertyValue("wd.passwd");;
	private static String output ="";
	private static String sURl = sProtocol + "://" + sWebUIHost + ":" + sPort + "/api/v2";
	public void processLineage(String original, String transformed) throws URISyntaxException {
		if(null==sSessionId){
			sSessionId=login();
			LOGGER.info("Logging into Waterlinedata Smart Catalog\n"+output);
		}
		String childKey = getKey(transformed, client, sSessionId);
		String parentKey =getKey(original, client, sSessionId);
		createLineage(client, sSessionId, parentKey, childKey);

	}
	
	NewCookie login()
	{	// logging in
        client=Client.create();
		String data = "{ \"username\": \"" + sUser + "\", \"password\": \""
				+ sPwd + "\" }";
		WebResource webResource2 = client.resource(sURl);
		if(null==sSessionId){
		ClientResponse response2 = webResource2.path("login")
				.type(MediaType.APPLICATION_JSON)
				.post(ClientResponse.class, data);
		output= response2.getEntity(String.class);
		List<NewCookie> cookie = response2.getCookies();
		for (int i = 0; i < cookie.size(); i++) {
			String dcook = cookie.get(i).toString();
			if (dcook.contains("WDSessionId")) {
				sSessionId = cookie.get(i);
			}
		}
		}
		
		return sSessionId;
		}	

	String getSearchPostBody(String resource) throws URISyntaxException{
		
		//add quotes for exact name match in Solr
        String path="\\\""+resource+"\\\"";
		return  "{\"searchPhrase\":\""
				+ path
				+ "\",\"facetSelections\":[],\"pagingCriteria\":{\"start\":0,\"size\":25},\"entityScope\":[]}";
	}
	
	
	String getKey(String resource,Client client,NewCookie sSessionId) throws URISyntaxException{
		String body =getSearchPostBody(resource);
		JSONObject jsonOne=null;
		WebResource webResource = client.resource(sURl);
		ClientResponse response = webResource.path("search/new")
				.header("Cookie", sSessionId).type(MediaType.APPLICATION_JSON)
				.post(ClientResponse.class, body);
		JSONObject jsonObj = new JSONObject(response.getEntity(String.class));
		JSONArray entityArray = jsonObj.getJSONArray("entities");
		if(entityArray.length()==0){
			LOGGER.error("Try Running Waterline Format Command before lineage");
			System.exit(-1);
		}
		
		else if(entityArray.length()>0){
		jsonOne = entityArray.getJSONObject(0);
		}
		return jsonOne.getString("key");
	}
	
	void createLineage(Client client,NewCookie sSessionId,String pKey,String cKey){
	try
	{
		String sData = "{ \"relatedKey\": \"" + pKey + "\"}";
		WebResource webResource1 = client.resource(sURl); 
		ClientResponse response1 = webResource1.path("lineage/addparent").path(cKey).header("Cookie", sSessionId).type(MediaType.APPLICATION_JSON).post(ClientResponse.class,sData);
		String output = response1.getEntity(String.class);
		LOGGER.info(output+ ":"+response1.getStatus());
	}
	catch (Exception ex)
	{
		ex.printStackTrace();
	}
	}
}
