package com.waterlinedata.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

import org.apache.log4j.Logger;

public class PropertiesUtil {
	private final static Logger LOGGER = Logger.getLogger(PropertiesUtil.class
			.getName());
	private static Properties prop;

	static {
		FileInputStream fis = null;
		try {
			File jarPath = new File(SchemaUtils.class.getProtectionDomain()
					.getCodeSource().getLocation().getPath());
			String propertiesPath = jarPath.getParentFile().getAbsolutePath();
			prop = new Properties();
			fis = new FileInputStream(propertiesPath + File.separator
					+ "config.properties");
			LOGGER.info("Trying to load properties ...");
			prop.load(fis);
		} catch (FileNotFoundException e) {
			LOGGER.error("Error finding config.properties");
			e.printStackTrace();
		} catch (IOException e) {
			LOGGER.error("IO Error encountered finding config.properties");
			e.printStackTrace();
		}
	}

	public static String getPropertyValue(String key) {
		return prop.getProperty(key);
	}
}
