package com.waterlinedata.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.RandomAccessFile;
import java.net.InetAddress;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FSDataInputStream;
import org.apache.hadoop.fs.FileStatus;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.log4j.Logger;

import com.waterlinedata.entity.Constants;

public class FileUtils {
	private final static Logger LOGGER = Logger.getLogger(FileUtils.class.getName());
	public static final String HEAD = PropertiesUtil.getPropertyValue("Postnummer.HEADER");
	public static final String TAIL = PropertiesUtil.getPropertyValue("Postnummer.TRAILER");
	static List<String> files = new ArrayList<String>();

	// recursively gets path to all the files in the given folder
	@SuppressWarnings("deprecation")
	public static List<String> getFilesList(String Url) throws IOException {
		FileSystem fs = null;
		try {
			Configuration conf = new Configuration();
			if (PropertiesUtil.getPropertyValue("datasource").equals(Constants.DS_HDFS)) {
				conf.set("fs.defaultFS", Constants.DS_HDFS + "://" + getHostName() + ":8020");
			} else if (PropertiesUtil.getPropertyValue("datasource").startsWith(Constants.DS_S3)) {
				conf.set("fs.defaultFS",PropertiesUtil.getPropertyValue("datasource"));
			}
			fs = FileSystem.get(URI.create(Url), conf);
			FileStatus[] status = fs.listStatus(new Path(Url));
			for (int i = 0; i < status.length; i++) {
				if (status[i].isDir()) {
					getFilesList(status[i].getPath().toString());
				} else {
					try {
						files.add(status[i].getPath().toString());
						LOGGER.info("Adding Files" + status[i].getPath().toString());
					} catch (Exception e) {
						System.err.println(e.toString());
					}
				}
			}
		} finally {
			if (fs != null)
				fs.close();
		}
		return files;
	}

	// get First and Last line in a file
	public String getPadding(Path path) throws IOException {
		File file = makeFileFromPath(path, new Configuration());
		RandomAccessFile fileHandler = null;
		try {
			fileHandler = new RandomAccessFile(file, "r");
			long fileLength = fileHandler.length() - 1;
			fileHandler.seek(0);
			String firstLine = fileHandler.readLine();
			StringBuilder sb = new StringBuilder();

			for (long filePointer = fileLength; filePointer != -1; filePointer--) {
				fileHandler.seek(filePointer);
				int readByte = fileHandler.readByte();

				if (readByte == 0xA) {
					if (filePointer == fileLength) {
						continue;
					}
					break;

				} else if (readByte == 0xD) {
					if (filePointer == fileLength - 1) {
						continue;
					}
					break;
				}

				sb.append((char) readByte);
			}

			String lastLine = sb.reverse().toString();
			return firstLine + "\n" + lastLine;
		} catch (java.io.FileNotFoundException e) {
			e.printStackTrace();
			return null;
		} catch (java.io.IOException e) {
			e.printStackTrace();
			return null;
		} finally {
			if (fileHandler != null)
				try {
					fileHandler.close();
				} catch (IOException e) {
					/* ignore */
				}
		}
	}

	public File makeFileFromPath(Path some_path, Configuration conf) throws IOException {
		FileSystem fs = FileSystem.get(some_path.toUri(), conf);
		File temp_data_file = File.createTempFile(some_path.getName(), "");
		temp_data_file.deleteOnExit();
		fs.copyToLocalFile(some_path, new Path(temp_data_file.getAbsolutePath()));
		return temp_data_file;
	}

	// TODO try processing by reading entire file into RDD
	public List<String> processFile(String url, String type) throws IllegalArgumentException, IOException {
		int min_record_length=Integer.valueOf(PropertiesUtil.getPropertyValue("min.record.length"));
		List<String> validRecords = new ArrayList<String>();
		if (type.equals(Constants.FileType_POSTNUMMER)) {
			Path filePath = new Path(url);
			FileSystem fs = FileSystem.get(URI.create(url), new Configuration());
			FSDataInputStream fsDataInputStream = fs.open(filePath);
			String out = IOUtils.toString(fsDataInputStream, "ISO-8859-1");
			/*
			 * LineIterator it =
			 * org.apache.commons.io.FileUtils.lineIterator(makeFileFromPath
			 * (filePath, new Configuration()), "ISO-8859-1"); try { while
			 * (it.hasNext()) { String line = it.nextLine(); // do something
			 * with line } } finally { LineIterator.closeQuietly(it); }
			 */
			String[] lines = out.split("\\r?\\n");
			for (String str : lines) {
				if (str.length() > min_record_length && !str.equals(HEAD) && !str.equals(TAIL))
					// if (str.length()>1&&!skipLines.contains(str))
					validRecords.add(str);
			}
		} else if (type.equals(Constants.FileType_ATX)) {
			Path filePath = new Path(url);
			FileSystem fs = FileSystem.get(URI.create(url), new Configuration());
			FSDataInputStream fsDataInputStream = fs.open(filePath);
			String out = IOUtils.toString(fsDataInputStream, "ISO-8859-1");
			String[] lines = out.split("\\r?\\n");
			for (String str : lines) {
				if (str.length() > min_record_length)
					validRecords.add(str);
			}
		}
		else if (type.equals(Constants.FileType_UNIREG)) {
			Path filePath = new Path(url);
			FileSystem fs = FileSystem.get(URI.create(url), new Configuration());
			FSDataInputStream fsDataInputStream = fs.open(filePath);
			String out = IOUtils.toString(fsDataInputStream, "ISO-8859-1");
			String[] lines = out.split("\\r?\\n");
			for (String str : lines) {
				if (str.length() > min_record_length)
					validRecords.add(str);
			}
		}
		return validRecords;
	}
	
	
	public static String padString(String str, int size, char padChar)
	{
	  StringBuilder padded = new StringBuilder(str);
	  while (padded.length() < size)
	  {
	    padded.append(padChar);
	  }
	  return padded.toString();
	}
	public static String getParentFolder(String file){
		File aFile=new File(file);
		return aFile.getParentFile().getName();
		
	}
	

	public String detectFile(String url) throws IllegalArgumentException, IOException {
		int atxCount = 0;
		int uniregCount = 0;
		int minRecords=5;
		String type = null;
		List<String> sampleRecords = new ArrayList<String>();
		int lineNumber = 0;
		Path filePath = new Path(url);
		String[] padding = getPadding(filePath).split("\\r?\\n");
		if (padding.length > 0) {
			if (padding[0].equals(HEAD) && padding[1].equals(TAIL)) {
				type = Constants.FileType_POSTNUMMER;
			} else {
				FileSystem fs = FileSystem.get(URI.create(url), new Configuration());
				BufferedReader br = new BufferedReader(new InputStreamReader(fs.open(filePath)));
				String line;
				while ((line = br.readLine()) != null && lineNumber < minRecords) {
					sampleRecords.add(line);
					lineNumber++;
				}
				for (String str : sampleRecords) {
					if (str.substring(0, 2).equals(str.substring(4, 6))) {
						atxCount++;
					} else if (str.substring(0, 6).contains("REG")) {
						uniregCount++;
					}
				}
				if (atxCount >= 4) {
					type = Constants.FileType_ATX;
				} else if (uniregCount >= 4) {
					type = Constants.FileType_UNIREG;
				}
			}
		}
		LOGGER.info("File " + url + " could be type " + type);
		return type;
	}

	public String getTestPath(String originalFile, String type, String value) {
		String transformedName = getTransformedFileName(originalFile, type, value);
		String originalFileName = getOriginalFileName(originalFile);
		return originalFile.replace(originalFileName, transformedName);
	}

	public String getOriginalFileName(String originalFile) {
		int start = originalFile.lastIndexOf("/");
		String originalFileName = originalFile.substring(start + 1, originalFile.length());
		return originalFileName;
	}

	public String getTransformedFileName(String originalFile, String type, String value) {
		String append = "";
		if (type.equals(Constants.FileType_ATX)|type.equals(Constants.FileType_UNIREG)) {
			if(value.equals("rejected"))
				append="_rejected";
			else
			append = "_transformed_" + value;
		} else {
			append = "_transformed";
		}
		String baseName=FilenameUtils.getBaseName(originalFile);
		String extension = null!=FilenameUtils.getExtension(originalFile)?FilenameUtils.getExtension(originalFile):"";

		return baseName + append+(""!=extension?"."+extension:"");
	}

	/*public String getTransformedFileName(String originalFileName, String type, String value) {
		String prefix = "";
		if (type.equals(Constants.FileType_ATX)) {
			prefix = Constants.FileType_ATX +"/"+ "v"+value+"/";
		} else {
			prefix = Constants.FileType_POSTNUMMER+"/";
		}
		return prefix+ originalFileName;
	}*/
	
	public String getNonTestFilePath(String originalFileName, String type, String value) {
	String fileName=getOriginalFileName(originalFileName);
	String prefix = getAuthority(originalFileName);
	if (type.equals(Constants.FileType_ATX)) {
		prefix += PropertiesUtil.getPropertyValue("atx.destination");
		prefix=prefix.replace("{year}", "20"+value).replace("{filename}", fileName);
	} 
	else if (type.equals(Constants.FileType_UNIREG)) {
		if(value.equals("rejected")){
			prefix+=PropertiesUtil.getPropertyValue("unireg.rejected").replace("{filename}", fileName);
		}
		else{
		prefix += PropertiesUtil.getPropertyValue("unireg.destination");
		prefix=prefix.replace("{posttype}", value).replace("{version}", getParentFolder(originalFileName)).replace("{filename}", fileName);
		}
	} 
	else {
		prefix += File.separator+Constants.FileType_POSTNUMMER+File.separator+fileName;
	}
	return prefix;
}
	
	public String getAuthority(String url){
		URI uri=URI.create(url);
		String separator="://";
		int start=0;
		int end=url.indexOf(separator);
		return url.substring(start,end)+separator+uri.getAuthority();
	}
	
	
	public static String getHostName() {
		String hostname = null;
		try {
			hostname = InetAddress.getLocalHost().getHostName();
		} catch (UnknownHostException e) {
			LOGGER.error("Error getting hostname");
			e.printStackTrace();
		}
		return hostname;

	}

	public static String getPath(String str) {
		String path = null;
		try {
			path = new URI(str).getPath();
		} catch (URISyntaxException e) {
			e.printStackTrace();
		}
		return path;
	}
}
