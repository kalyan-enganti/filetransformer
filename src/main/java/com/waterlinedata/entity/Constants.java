package com.waterlinedata.entity;

public class Constants {
public static String ACTION_TRANSFORM = "transform";
public static String ACTION_LINEAGE="lineage";
public static String FileType_POSTNUMMER="Postnummer";
public static String FileType_ATX="atx";
public static String FileType_UNIREG="unireg";
public static String DS_HDFS="hdfs";
public static String DS_S3="s3";
public static String DS_gs="gs";
}
