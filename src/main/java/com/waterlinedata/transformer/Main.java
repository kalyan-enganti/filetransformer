package com.waterlinedata.transformer;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.List;

import com.waterlinedata.entity.Constants;
import com.waterlinedata.util.FileUtils;
import com.waterlinedata.util.PropertiesUtil;
import com.waterlinedata.util.CacheUtil;
import org.apache.log4j.Logger;

public class Main {
	private final static Logger LOG = Logger.getLogger(Main.class.getName());

	public static void main(String[] args) throws URISyntaxException, IOException {
		createLogFile();
		if (args.length < 2) {
			LOG.error("Arguments mismatch");
			System.out.println(getUsage());
			System.exit(-1);
		}

		String action = args[0];
		String uri = args[1];

		List<String> filesList = FileUtils.getFilesList(uri);

		if (action.equals(Constants.ACTION_TRANSFORM)) {
			FileTransformer.transformFiles(filesList);
			// MTFT.transformFiles(filesList);
		} else if (action.equals(Constants.ACTION_LINEAGE)) {
			for (String str : filesList) {
				CacheUtil.readFromCache(str);
			}
		} 
		else {
			LOG.error("Invalid action specified");
			System.out.println(getUsage());
			System.exit(-1);
		}

	}

	private static void createLogFile() {
		String dir = PropertiesUtil.getPropertyValue("log.file.loc");
		String fileName = "ft.log";
		File f = new File(dir, fileName);
		if (!f.exists()) {
			LOG.info("First run....Creating log file at "+dir);
			f.mkdirs();
		}
		else{return;}

	}

	static String getUsage() {
		return "Usage : java -jar FileTransformer.jar  action pathToFileOrFolder\n" + "action - [transform , lineage]\n"
				+ "pathToFileOrFolder - [path to resource on hdfs]";

	}
}
