package com.waterlinedata.transformer;

import static org.apache.spark.sql.functions.concat;
import static org.apache.spark.sql.functions.lit;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.net.URI;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FSDataOutputStream;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.log4j.Logger;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.api.java.function.Function;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SQLContext;
import org.apache.spark.sql.SaveMode;
import org.apache.spark.sql.types.DataTypes;
import org.apache.spark.sql.types.StructField;
import org.apache.spark.sql.types.StructType;

import com.waterlinedata.entity.Constants;
import com.waterlinedata.util.CacheUtil;
import com.waterlinedata.util.FileUtils;
import com.waterlinedata.util.PropertiesUtil;
import com.waterlinedata.util.SchemaUtils;

@SuppressWarnings("deprecation")
public class FileTransformer {
	private final static Logger LOGGER = Logger.getLogger(FileTransformer.class.getName());
	private static String colNames[] = null;
	public static final int UNIREG_RECORD_SIZE=410;
	public static final char BLANK=' ';
	private static Integer fieldLengths[] = null;
	private static String type;
	private static final String delimiter = ",";
	private static boolean isInitial;
	private static FileUtils fUtils = new FileUtils();
	private static JavaSparkContext sparkContext;
	private static SQLContext sqlContext;
	private static SparkConf sparkConfig;
	static String record;
	static List<String> recordsList = new ArrayList<String>();
	private static Date start;

	static public void setHeader() {
		colNames = SchemaUtils.getColNames();
		fieldLengths = SchemaUtils.getFieldLengths();
	}

	static {
		sparkConfig = new SparkConf();
		sparkConfig.setAppName("FileTransformer");
		sparkConfig.setMaster("local[*]");
		sparkConfig.set("spark.driver.allowMultipleContexts", "true");
		sparkContext = new JavaSparkContext(sparkConfig);
		sqlContext = new SQLContext(sparkContext);
	}

	@SuppressWarnings({ "serial" })
	static void transformFiles(List<String> filesList) {
		LOGGER.info("Started Application with appId: " + 
				sparkContext.getConf().getAppId());
		for (String str : filesList) {
			LOGGER.info("Attempting to transform file " + str);
			String schemaData[] = null;
			// detect file type
			try {
				type = fUtils.detectFile(str);
			} catch (IllegalArgumentException e1) {
				LOGGER.error("Illegal argument Exception trying to detect file type");
				e1.printStackTrace();
			} catch (IOException e1) {
				LOGGER.error("IO Exception trying to detect file type");
				e1.printStackTrace();
			}

			if (null == type) {
				LOGGER.info("Skipping file :" + FileUtils.getPath(str));
				continue;
			}

			if (type.equalsIgnoreCase(Constants.FileType_POSTNUMMER)) {
				schemaData = SchemaUtils.getFileSchemaData(type, null,null);
				SchemaUtils.setFieldLengthsAndNames(schemaData);
				setHeader();
			}
			List<String> processedlist = null;
			List<String> uniregList = null;
			List<String> nonuniregList = null;
			JavaRDD<String> inputFile = null;
			List<StructField> fields = new ArrayList<StructField>();
			start = new Date();
			try {
				processedlist = fUtils.processFile(str, type);
			} catch (IllegalArgumentException e) {
				LOGGER.error("Illegal argument Exception trying to get processedList");
				e.printStackTrace();
			} catch (IOException e) {
				LOGGER.error("IOException trying to get processedList");
				e.printStackTrace();
			}

			// separate list of rows based on unireg and non-unireg

			if (type.equals(Constants.FileType_UNIREG)) {
				uniregList = new ArrayList<String>();
				nonuniregList = new ArrayList<String>();
				for (String string : processedlist) {
					if (string.startsWith("UNIREG")) {
						uniregList.add(FileUtils.padString(string,UNIREG_RECORD_SIZE,BLANK));
					} else {
						nonuniregList.add(string);
					}
				}
				if (uniregList.size() > 0) {
					LOGGER.info("Process unireg records");
				} else if (uniregList.size() == 0) {
					LOGGER.info("No unireg records found...skipping file");
					return;

				}

			}

			if (!type.equals(Constants.FileType_UNIREG)) {
				inputFile = sparkContext.parallelize(processedlist);
				if (type.equalsIgnoreCase(Constants.FileType_POSTNUMMER)) {
					// create column names
					for (String fieldName : colNames) {
						fields.add(DataTypes.createStructField(fieldName, DataTypes.StringType, true));
					}
				}
				else if (type.equals(Constants.FileType_ATX)) {
					// create column names
					isInitial = true;
					fields.add(DataTypes.createStructField("year", DataTypes.StringType, true));
					fields.add(DataTypes.createStructField("data", DataTypes.StringType, true));
					fieldLengths = new Integer[2];
					fieldLengths[0] = 2;
					fieldLengths[1] = 337;
				}
			}
			
			else if (type.equals(Constants.FileType_UNIREG)) {
				inputFile = sparkContext.parallelize(uniregList);
				// create column names
				isInitial = true;
				fields.add(DataTypes.createStructField("prefix", DataTypes.StringType, true));
				fields.add(DataTypes.createStructField("post_type", DataTypes.StringType, true));
				fields.add(DataTypes.createStructField("suffix", DataTypes.StringType, true));
				fieldLengths = new Integer[3];
				fieldLengths[0] = 66;
				fieldLengths[1] = 3;
				fieldLengths[2] = 60;
			}

			StructType customSchema = DataTypes.createStructType(fields);
			JavaRDD<Row> rowRDD = SchemaUtils.createRDD(type, isInitial, inputFile, fieldLengths, colNames);

			// Apply the schema to the RDD.
			Dataset<Row> df = sqlContext.createDataFrame(rowRDD, customSchema);
			df.cache();
			df.registerTempTable("data");
			//df.show();
			// ATX-MultiSchema specific process
			if (type.equals(Constants.FileType_ATX)) {
				record = new String(FileUtils.getPath(str) + "\t");
				Dataset<Row> years = sqlContext.sql("SELECT DISTINCT year FROM data");
				// years.show();
				List<Row> yearsList = years.collectAsList();
				for (Row row : yearsList) {
					String value = row.toString().replace("[", "").replace("]", "");
					Dataset<Row> df1 = sqlContext.sql("SELECT * FROM data where year = '" + row.get(0) + "'");

					schemaData = SchemaUtils.getFileSchemaData(type, value,null);
					df1.registerTempTable("tempTable");
					Dataset<Row> data = // get data
							df1.withColumn("new_data", concat(df1.col("year"), lit(""), df1.col("data"))).drop("year")
									.drop("data");
					data.cache();

					JavaRDD<String> inputRows = data.javaRDD().map(new Function<Row, String>() {
						public String call(Row row) throws Exception {
							return (String) row.get(0);
						}
					});

					// recreate schema for concatenated column values
					fields = new ArrayList<StructField>();
					SchemaUtils.setFieldLengthsAndNames(schemaData);
					setHeader();
					for (String fieldName : colNames) {
						fields.add(DataTypes.createStructField(fieldName, DataTypes.StringType, true));
					}
					// /recreate schema for concatenated columns//
					customSchema = DataTypes.createStructType(fields);
					JavaRDD<Row> finalRDD = SchemaUtils.createRDD(type, false, inputRows, fieldLengths, colNames);
					// recreate schema bit ends here

					Dataset<Row> dataFrame = sqlContext.createDataFrame(finalRDD, customSchema);
					// dataFrame.show();
					String destinationFileName = "";
					if(PropertiesUtil.getPropertyValue("env").equals("test")){
						destinationFileName = fUtils.getTestPath(str, type, value);
					}
					else if(PropertiesUtil.getPropertyValue("env").equals("dev")||PropertiesUtil.getPropertyValue("env").equals("prod")){
						destinationFileName = fUtils.getNonTestFilePath(str, type, value);
	
					}
					record += FileUtils.getPath(destinationFileName) + delimiter;
					dataFrame.select("*").coalesce(1).write().format("csv").option("header", "true")
							.mode(SaveMode.Overwrite).save(destinationFileName);
				}
			} else if (type.equals(Constants.FileType_UNIREG)) {
				record = new String(FileUtils.getPath(str) + "\t");
				//Dataset<Row> POST_TYPE = sqlContext.sql("SELECT DISTINCT post_type FROM data");
				Dataset<Row> POST_TYPE = sqlContext.sql("SELECT DISTINCT post_type FROM data");

				List<Row> postList = POST_TYPE.collectAsList();
				for (Row row : postList) {
					String value = row.toString().replace("[", "").replace("]", "").trim();
					Dataset<Row> df1 = sqlContext.sql("SELECT * FROM data where post_type = '" + row.get(0) + "'");
					schemaData = SchemaUtils.getFileSchemaData(type, value,str);
					df1.registerTempTable("tempTable");
					//df1.show();
					Dataset<Row> data = // get data
							df1.withColumn("new_data", concat(df1.col("prefix"), lit(""), df1.col("post_type"), lit(""), df1.col("suffix"))).drop("prefix")
									.drop("post_type").drop("suffix");
					data.cache();
                   // data.show();
					JavaRDD<String> inputRows = data.javaRDD().map(new Function<Row, String>() {
						public String call(Row row) throws Exception {
							return (String) row.get(0);
						}
					});

					// recreate schema for concatenated column values
					fields = new ArrayList<StructField>();
					SchemaUtils.setFieldLengthsAndNames(schemaData);
					setHeader();
					for (String fieldName : colNames) {
						fields.add(DataTypes.createStructField(fieldName, DataTypes.StringType, true));
					}
					// /recreate schema for concatenated columns//
					customSchema = DataTypes.createStructType(fields);
					JavaRDD<Row> finalRDD = SchemaUtils.createRDD(type, false, inputRows, fieldLengths, colNames);
					// recreate schema bit ends here

					Dataset<Row> dataFrame = sqlContext.createDataFrame(finalRDD, customSchema);
					//dataFrame.show();
					String destinationFileName="";
					if(PropertiesUtil.getPropertyValue("env").equals("test")){
						destinationFileName = fUtils.getTestPath(str, type, value);
					}
					else if(PropertiesUtil.getPropertyValue("env").equals("dev")||PropertiesUtil.getPropertyValue("env").equals("prod")){
						destinationFileName = fUtils.getNonTestFilePath(str, type, value);
	
					}
					record += FileUtils.getPath(destinationFileName) + delimiter;
					dataFrame.select("*").coalesce(1).write().format("csv").option("header", "true")
							.mode(SaveMode.Overwrite).save(destinationFileName);
				}
				//write file with rejected records to hdfs
				if(nonuniregList.size()>0&&uniregList.size()>0){
					String destinationFileName = "";
					if(PropertiesUtil.getPropertyValue("env").equals("test")){
						destinationFileName = fUtils.getTestPath(str, type, "rejected");
					}
					else if(PropertiesUtil.getPropertyValue("env").equals("dev")||PropertiesUtil.getPropertyValue("env").equals("prod")){
						destinationFileName = fUtils.getTestPath(str, type, "rejected");
					}
					
					FileSystem fs = null;
					try {
						fs = FileSystem.get(URI.create(destinationFileName), new Configuration());
					} catch (IOException e) {
						LOGGER.error("IOException trying to create "+ destinationFileName);
						e.printStackTrace();
					}
					Path path=new Path(destinationFileName);
					FSDataOutputStream out=null;
					try {
						out = fs.create(path,true);
					} catch (IOException e) {
						e.printStackTrace();
					}
					BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(out));
					for(String record:nonuniregList){						
						try {
							bw.write(record+System.lineSeparator());
						} catch (IOException e) {
							LOGGER.error("IOException trying to write data to "+ destinationFileName);
						}
					}					
					   try{
					      if(bw!=null)
						 bw.close();
					   }catch(Exception ex){
							LOGGER.error("Error closing buffered Writer");
					    }
					record += FileUtils.getPath(destinationFileName) + delimiter;
				}
			}
			// Postnummer specific
			else {
				String destinationFileName = "";
				if(PropertiesUtil.getPropertyValue("env").equals("test")){
					destinationFileName = fUtils.getTestPath(str, type, "");
				}
				else if(PropertiesUtil.getPropertyValue("env").equals("dev")||PropertiesUtil.getPropertyValue("env").equals("prod")){
					destinationFileName = fUtils.getNonTestFilePath(str, type, "");
				}
				
				record = new String(FileUtils.getPath(str) + "\t");
				record += FileUtils.getPath(destinationFileName);
				df.select("*").coalesce(1).write().format("com.databricks.spark.csv").option("header", "true")
						.mode(SaveMode.Overwrite).save(destinationFileName);
			}
			recordsList.add(record);
			Date end = new Date();
			long ms = end.getTime() - start.getTime();
			LOGGER.info("Processed File " + str + " in" + ms + " millisecs");

		}

		CacheUtil.writeToCache(recordsList);

	}

	static int indexOf(Pattern pattern, String s) {
		Matcher matcher = pattern.matcher(s);
		return matcher.find() ? matcher.start() : -1;
	}

}
